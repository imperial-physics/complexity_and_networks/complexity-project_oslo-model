import simulation
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import  curve_fit


def func(time, a, b, c):
	return(a*time**b + c)

def log_fit(time, grad, intercept):
	return(grad * np.array(time) + intercept)

def data_collapse(ensemble):

	height, ax = plt.subplots()
	ax.set_xlabel("Time")
	ax.set_ylabel("Height")

	for sim in ensemble:
		ax.plot(sim.scaled_time(), sim.scaled_height())
		#ax.axvline(x=sim.recurrance_time, linewidth=((sim.time)**0.5/50 + 0.25))

	height.show()

def smooth_data_collapse(ensemble, period=50):

	height, ax = plt.subplots()
	loglog, ax2 = plt.subplots()
	ax.set_xlabel(r"Natural time, $\alpha$")
	ax.set_ylabel(r"Time averaged slope $\tilde{z}(t;L)$")
	ax2.set_xlabel(r"Natural time, $\alpha$")
	ax2.set_ylabel(r"Time averaged slope $\tilde{z}(t;L)$")


	for sim in ensemble:
		ax.plot(sim.scaled_time(period=period), sim.scaled_height(sim.time_average(sim.find_pile_height(),period=period)), label="L = %s" % sim.board.board_size)
		ax2.loglog(sim.scaled_time(period=period), sim.scaled_height(sim.time_average(sim.find_pile_height(),period=period)), label="L = %s" % sim.board.board_size)
		ax.axvline(x=sim.recurrence_time/sim.board.board_size**2, linewidth=0.25)

	ax2.loglog(np.logspace(-4,1,num=5, endpoint=True),  1.828*(np.logspace(-4,1,num=5, endpoint=True))**0.5, "b--")
	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()
	height.show()
	loglog.show()

def smooth_data_collapse_empirical(ensemble, period=50):

	height, ax = plt.subplots()
	loglog, ax2 = plt.subplots()
	ax.set_xlabel(r"Natural time, $\alpha$")
	ax.set_ylabel(r"Time averaged slope $\tilde{z}(t;L)$")
	ax2.set_xlabel(r"Natural time, $\alpha$")
	ax2.set_ylabel(r"Time averaged slope $\tilde{z}(t;L)$")


	for sim in ensemble:
		ax.plot(np.arange(period, sim.time - period)/sim.board.board_size**2.0145, sim.scaled_height(sim.time_average(sim.find_pile_height(),period=period)), label="L = %s" % sim.board.board_size)
		ax2.loglog(np.arange(period, sim.time - period)/sim.board.board_size**2.0145, np.array(sim.time_average(sim.find_pile_height(),period=period))/sim.board.board_size**1.01418, label="L = %s" % sim.board.board_size)
		ax.axvline(x=sim.recurrence_time/sim.board.board_size**2, linewidth=0.25)

	ax2.loglog(np.logspace(-4,1,num=5, endpoint=True),  1.828*(np.logspace(-4,1,num=5, endpoint=True))**0.5, "b--")
	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()
	height.show()
	loglog.show()

def transient_power_law(ensemble, period = 50):

	height, ax = plt.subplots()
	loglog, ax2 = plt.subplots()
	ax.set_xlabel("Time")
	ax.set_ylabel("Height")


	for sim in ensemble:
		ax.plot(sim.scaled_time(period=period), sim.scaled_height(sim.time_average(sim.find_pile_height(),period=period)), label="L = %s" % sim.board.board_size)
		ax2.loglog(sim.scaled_time(period=period), sim.scaled_height(sim.time_average(sim.find_pile_height(),period=period)), label="L = %s" % sim.board.board_size)
		
		power, err = curve_fit(log_fit, np.log(range(1,sim.recurrence_time)),np.log(sim.find_pile_height()[1:sim.recurrence_time]))
		#ax.plot(sim.scaled_time(sim.recurrence_time), sim.scaled_height(func(range(sim.recurrence_time), power[0],power[1],power[2])), label="Fit of L = %s" % sim.board.board_size)
		print("Transient height increases with time to the power", power[0], "+-", err[0,0]**0.5)
		#ax.axvline(x=sim.recurrence_time/sim.board.board_size**2, linewidth=0.25)

	height.show()
	loglog.show()