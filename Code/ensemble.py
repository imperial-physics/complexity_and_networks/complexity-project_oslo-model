import simulation
import task1
import task2a
import task2b
import task2c
import task2d
import task3a
import task3b
import task3c
import task3d
import extension1

def initalise_ensemble(board_sizes, p = 0.5, N=10**6, until_recurrence=True):

	simulations = [simulation.Simulation(board_size, threshold_prob = p) for board_size in board_sizes]
	average_heights = []

	for sim in simulations:
		print("Simulating board of size ", sim.board.board_size, sep="")
		if until_recurrence == True:
			sim.simulate(until_recurrence=True)
		sim.simulate(N)

	return(simulations)