import simulation
import matplotlib.pyplot as plt
import numpy as np
import log_bin3
from scipy.optimize import curve_fit

def x_scaling(avalanche_sizes, board_size):
	avalanche_dimension = 2.25
	return(np.array(avalanche_sizes)/(board_size**avalanche_dimension))

def y_scaling(avalanche_prob_density, avalanche_size):
	avalanche_size_exponent = 1.5465
	return(np.array(avalanche_prob_density) * np.array(avalanche_size)**avalanche_size_exponent)

def linear_fit(x, a, c):
	return(a * np.array(x) + c)

def get_prob_density(sim, a=1.3):
	avalanche_size = sim.recurrent_avalanches()
	avalanche_probabilities = sim.avalanche_probabilities()
	bin_centres, prob_density = log_bin3.log_bin(avalanche_size, a=a, datatype="integer", drop_zeros=False)
	return(bin_centres, prob_density)

def plot_probability(ensemble, a=1.3):
	figure, ax = plt.subplots()
	ax.set_xlabel(r"$\frac{s}{L^D}$")
	ax.set_ylabel(r"$s^{\tau_s}\tilde{P}_N(s,L)$")
	ax.set_yscale('log')
	ax.set_xscale('log')

	for sim in ensemble:

		bin_centres, prob_density = get_prob_density(sim, a=a)

		##Binning the data using given code
		ax.plot(x_scaling(bin_centres, sim.board.board_size), y_scaling(prob_density, bin_centres), label=r"$L = %s$" % sim.board.board_size)


	ax.legend(loc='best').draggable()
	figure.show()

def fit_avalanche_dimension(ensemble, a=1.3):
	figure, ax = plt.subplots()
	ax.set_xlabel(r"$L$")
	ax.set_ylabel(r"$s_c$")
	ax.set_yscale('log')
	ax.set_xscale('log')

	board_sizes = [sim.board.board_size for sim in ensemble]
	peaks=[]

	for sim in ensemble:

		bin_centres, prob_density = get_prob_density(sim, a=a)
		scaled_prob = y_scaling(prob_density, bin_centres)
		s_c = bin_centres[list(scaled_prob).index(max(scaled_prob[1:]))]
		peaks.append(s_c)
		ax.plot(bin_centres, scaled_prob)
		ax.scatter(s_c, max(scaled_prob[1:]))


	D_fit = curve_fit(linear_fit, np.log(board_sizes), np.log(peaks))
	print(D_fit)
	print("D =", D_fit[0][0], "+-", D_fit[1][0,0]**0.5)
	ax.plot(board_sizes, peaks)
	figure.show()


def fit_scalefree_exponent(sim, cutoff_size):
	"""
	Applies a linear fit for the scale invariant portion of the avalanche probability distribution to calculate tau_s
	Cut off avalanche sizes
	8: N/A
	16: 48
	32: 170
	64: 800
	128: 3850
	256: 19000
	512: 85000

	"""
	figure, ax = plt.subplots()
	ax.set_xlabel("Avalanche size")
	ax.set_ylabel("Avalanche probability")
	ax.set_yscale('log')
	ax.set_xscale('log')

	lower_cutoff = 5
	upper_cutoff = cutoff_size
	avalanche_range = range(lower_cutoff, upper_cutoff)
	bin_centres, prob_density = get_prob_density(sim, a=1.3)

	for index, value in enumerate(bin_centres):
		if value > lower_cutoff:
			bin_min = index
			break
	for index, value in enumerate(bin_centres[::-1]):
		if value < upper_cutoff:
			bin_max = len(bin_centres) - index
			break

	bin_centres_restricted = bin_centres[bin_min:bin_max]
	prob_density_restricted = prob_density[bin_min:bin_max]

	print(bin_centres)
	print(bin_centres_restricted)

	ax.plot(bin_centres,prob_density, label="Probability density")

	ax.plot(bin_centres_restricted,prob_density_restricted, label="Scale free domain")

	fit = curve_fit(linear_fit, np.log(bin_centres_restricted), np.log(prob_density_restricted))

	tau_s = fit[0][0]
	tau_s_error = fit[1][0,0]**0.5
		

	ax.legend(loc='best').draggable()
	figure.show()
	return(tau_s, tau_s_error)
