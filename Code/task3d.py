import simulation
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.optimize import  curve_fit

def log_fit(log_size, gradient, intercept):
	return(gradient * np.array(log_size) + intercept)

def plot_moments(ensemble, k_max=5):
	figure1, ax = plt.subplots()
	ax.set_xlabel("Avalanche size")
	ax.set_ylabel("Avalanche probability")
	ax.set_yscale('log')
	ax.set_xscale('log')
	figure2, ax2 = plt.subplots()
	ax2.set_xlabel("$k$")
	ax2.set_ylabel(r'$D(1 + k - \tau_s)$')

	k_values = range(1,k_max+1)
	board_sizes = [sim.board.board_size for sim in ensemble]
	moments = [list([]) for _ in k_values]

	##sorts data to give lists of a moment as a function of system size
	## ie. moments[k-1][board_index] gives the kth moment for a particular boardsize
	for k in k_values:
		for sim in ensemble:
			moments[k-1].append(sim.avalanche_k_moment(k=k))


	## plots the kth moment against system size for all k
	for moment in moments:
		ax.plot(board_sizes, moment, label=r"$\langle s^{%s} \rangle$" % str(moments.index(moment) + 1))

	log_gradients = []
	log_gradient_err = []
	log_moments = np.log(np.array(moments))

	## Calculates the gradient and its uncertainty for the log of each kth moment.
	for moment in log_moments:
		z, z_err=curve_fit(log_fit, np.log(np.array(board_sizes)),moment)
		log_gradients.append(z[0])
		log_gradient_err.append(math.sqrt(z_err[0,0]))

	#print(log_gradient_err)

	## Plots the gradient of the kth moment against k, should return a straight line.
	ax2.plot(k_values, log_gradients, label=r'$D(1 + k - \tau_s)$')

	## Calculates linear fit of $D*k + c$. Data points are weighted by their errors
	fit, error = curve_fit(log_fit, k_values, log_gradients, sigma=log_gradient_err, absolute_sigma=True)
	D, intercept = fit
	D_error, intercept_error = error[0,0]**0.5 ,  error[1,1]**0.5
	## Overlays linear fit onto previous axes
	ax2.plot(k_values, D * (k_values) + intercept, label=r'$%f \times k %f$' % (D , intercept))

	print("D =", D, "+-", D_error)
	print("Tau_s =", 1 - intercept/D, "+-", ((intercept*D_error/D**2)**2+(intercept_error/D)**2)**0.5)


	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()

	figure1.show()
	figure2.show()

def plot_moments_cts(ensemble, k_max=5):
	figure1, ax = plt.subplots()
	ax.set_xlabel("Avalanche size")
	ax.set_ylabel("Avalanche probability")
	ax.set_yscale('log')
	ax.set_xscale('log')
	figure2, ax2 = plt.subplots()
	ax2.set_xlabel(r"$k$")
	ax2.set_ylabel(r'$D(1 + k - \tau_s)$')
	figure3, ax3 = plt.subplots()
	ax3.set_xlabel(r"$L$")
	ax3.set_ylabel(r"$L^{-D(1+k-\tau_s)} \langle s^k \rangle$")
	ax3.set_yscale('log')
	ax3.set_xscale('log')

	k_values = range(1,k_max+1)
	board_sizes = [sim.board.board_size for sim in ensemble]
	moments = [list([]) for _ in k_values]

	##sorts data to give lists of a moment as a function of system size
	## ie. moments[k-1][board_index] gives the kth moment for a particular boardsize
	for k in k_values:
		for sim in ensemble:
			moments[k-1].append(sim.avalanche_k_moment(k=k))


	## plots the kth moment against system size for all k
	for moment in moments:
		ax.plot(board_sizes, moment, label=r"$\langle s^{%s} \rangle$" % str(moments.index(moment) + 1))

	log_gradients = []
	log_gradient_err = []
	log_moments = np.log(np.array(moments))

	## Calculates the gradient and its uncertainty for the log of each kth moment.
	for moment in log_moments:
		z, z_err=curve_fit(log_fit, np.log(board_sizes[-4:]),moment[-4:])
		print(z)
		print(z_err)
		log_gradients.append(z[0])
		log_gradient_err.append(math.sqrt(z_err[0,0]))

	for moment in moments:
		ax3.plot(board_sizes, np.array(moment)/(np.array(board_sizes)**log_gradients[moments.index(moment)]), label=r"$\langle s^{%s} \rangle$" % str(moments.index(moment) + 1))
	
	print(log_gradients)

	## Plots the gradient of the kth moment against k, should return a straight line.
	ax2.plot(k_values, log_gradients, label=r'$D(1 + k - \tau_s)$')


	## Calculates linear fit of $D*k + c$. Data points are weighted by their errors
	fit, error = curve_fit(log_fit, k_values, log_gradients, sigma=log_gradient_err, absolute_sigma=True)
	D, intercept = fit
	D_error, intercept_error = error[0,0]**0.5 ,  error[1,1]**0.5
	## Overlays linear fit onto previous axes
	ax2.plot(k_values, D * (k_values) + intercept, label=r'$%f \times k %f$' % (D , intercept))

	print("D =", D, "+-", D_error)
	print("Tau_s =", 1 - intercept/D, "+-", ((intercept*D_error/D**2)**2+(intercept_error/D)**2)**0.5)


	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()
	ax3.legend(loc='best').draggable()

	figure1.show()
	figure2.show()
	figure3.show()