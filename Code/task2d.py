import simulation
import matplotlib.pyplot as plt
import numpy as np
import itertools

colours = itertools.cycle(['b','g','r','c','m','y','k'])

def heights_vs_system_size(ensemble):

	prob, ax = plt.subplots()
	ax.set_xlabel(r"Pile height, $h$")
	ax.set_ylabel(r"Height probability, $P(h;L)$")

	for sim in ensemble:
		heights = range(min(sim.get_recurrence_heights()),max(sim.get_recurrence_heights())+1)
		ax.plot(heights, sim.height_probabilities(),"o-", label=r"$L = %s$" % sim.board.board_size)


	ax.legend(loc='best').draggable()

	prob.show()

def data_collapse(ensemble):

	prob, ax = plt.subplots()
	ax.set_xlabel(r"Height deviation, $\tilde{h}$")
	ax.set_ylabel(r"$\sigma_h P(\tilde{h})$")

	for sim in ensemble:
		heights = range(min(sim.get_recurrence_heights()),max(sim.get_recurrence_heights())+1)
		scaled_heights = (np.array(heights)-np.array(sim.average_h_power_k()))/sim.standard_deviation()
		scaled_probs = np.array(sim.height_probabilities())*sim.standard_deviation()

		ax.plot(scaled_heights, scaled_probs, 'o', label=r"$L = %s$" % sim.board.board_size)
		#ax.scatter(scaled_heights, scaled_probs, color=next(colours))

	ax.legend(loc='best').draggable()
	prob.show()


