import simulation
import matplotlib.pyplot as plt
import numpy as np

board_sizes = [8]



def plot_heights(ensemble):

	height, ax = plt.subplots()
	loglog, ax2 = plt.subplots()
	
	ax.set_xlabel("Time")
	ax.set_ylabel("Height")
	ax2.set_xlabel("Time")
	ax2.set_ylabel("Height")

	for sim in ensemble:
		ax.plot(range(sim.time), sim.find_pile_height(), label=r"$p = %s$" % sim.board.threshold_prob)
		ax2.loglog(range(sim.time), sim.find_pile_height(), label=r"$p = %s$" % sim.board.threshold_prob)
		ax.axvline(x=sim.recurrence_time, linewidth=0.5)

	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()
	height.show()
	loglog.show()

#Removed
def test_avalanches(ensemble):

	avalanches, ax = plt.subplots()
	ax.set_xlabel("Time")
	ax.set_ylabel("Avalanche size")

	for sim in ensemble[::2]:
		ax.plot(range(sim.time), sim.avalanches(),label=r"$p = %s$" % sim.board.threshold_prob)
		ax.axvline(x=sim.recurrence_time, linewidth=((sim.time)**0.5/50 + 0.25), color="r")


	ax.legend(loc='best').draggable()
	avalanches.show()