import random

class Board():
	"""
	Class representing a single instance of the Oslo Model
	"""
	def __init__ (self, board_size, threshold_prob=0.5):
		"""
		Initialises Board

		board_size: number of columns of the Oslo model being simulated
		threshold_prob: probability that the threshold slope will change after relaxation
		"""
		self.board_size = board_size
		self.threshold_prob = threshold_prob
		self.columns = [Column(self, column_num) for column_num in range(self.board_size)]
		
		## Replaces first and last columns to accomodate the special cases
		self.columns[0], self.columns[-1] = First_Column(self), Last_Column(self)
		self.time = 0
		self.pile_height = 0
		self.recurrence_flag = False
		self.outflow_counter = 0
		self.refresh_boardstate()

	def increase_pile_height(self):
		self.pile_height += 1

	def decrease_pile_height(self):
		self.pile_height -= 1

	def drive(self):
		"""
		Driving step of the Oslo Model.
		Increases the slope of the first site by 1 and refreshes the board state
		"""
		self.columns[0].increase_slope()
		self.increase_pile_height()
		#self.refresh_boardstate()


	def find_relaxs(self):
		"""
		Finds unstable sites.
		Returns a list showing whether each site is stable or not
		"""
		relax_sites = []
		for column in self.columns:
			if column.unstable():
				relax_sites.append(True)
			else:
				relax_sites.append(False)
		return(relax_sites)


	def relax_board(self):
		"""
		Relaxes the board.
		Uses self.find_relaxs() to find unstable sites, then calls relax() method on leftmost unstable site.
		"""
		try:
			self.columns[self.find_relaxs().index(True)].relax()
			#self.refresh_boardstate()
			return(1)
		except ValueError:
			return(0)
		
	def refresh_boardstate(self):
		"""
		Reads site slope and threshold slope of each site to show board state
		Should run after every time tick.
		"""
		self.board_slopes = []
		self.board_thresholds = []
		for i in range(self.board_size):
			self.board_slopes.append(self.columns[i].slope)
			self.board_thresholds.append(self.columns[i].threshold)

	def increment_outflow(self):
		self.outflow_counter += 1

	def read_outflow(self):
		return(self.outflow_counter)

	def reset_outflow(self):
		self.outflow_counter = 0

	def show_board(self):
		"""
		Prints board state
		"""
		self.refresh_boardstate()
		print("Slopes:")
		print(self.board_slopes)
		print("Thresholds:")
		print(self.board_thresholds)
		print("Pile Height:")
		print(self.pile_height, "\n")

	def recurrence(self):
		self.recurrence_flag = True

class Column():

	def __init__ (self, parent, column_num):
		"""
		Initialises a Column

		self.parent:the Board on which the column is on
		self.column_num: the position on the parent Board
		"""
		self.parent = parent
		self.column_num = column_num
		self.slope = 0
		self.change_threshold(self.parent.threshold_prob)



	def change_threshold(self, p):
		"""
		Changes threshold slope to be 1 with probability p, 2 with probabilty 1-p
		"""
		if random.random() < p:
			self.threshold = 1
		else:
			self.threshold = 2

	def unstable(self):
		"""
		Tests for site stability (self.slope > self.threshold)
		Returns True if unstable
		"""
		if self.slope > self.threshold:
			return True
		else:
			return False

	def relax(self):
		"""
		Relaxes site in accordance with Oslo Model.
		"""
		self.parent.columns[self.column_num - 1].increase_slope()
		self.parent.columns[self.column_num + 1].increase_slope()
		self.decrease_slope()
		self.change_threshold(self.parent.threshold_prob)

	def increase_slope(self):
		"""
		Increases slope by 1
		"""
		self.slope += 1

	def decrease_slope(self):
		"""
		Decreases the slop by 2. If site is the last site on the board then it is reduced by 1
		"""
		self.slope -= 2

class First_Column(Column):
	"""
	Represents the final Column on the Board.
	decrease_slope() is adjusted for the special behaviour of this site. 
	"""

	def __init__(self, parent):
		Column.__init__(self, parent, 0)

	def relax(self):
		"""
		Relaxes site in accordance with Oslo Model.
		"""
		self.parent.columns[1].increase_slope()
		# Decrease pile height when first site relaxes
		self.parent.decrease_pile_height()
		self.decrease_slope()
		self.change_threshold(self.parent.threshold_prob)

class Last_Column(Column):
	"""
	Represents the final Column on the Board.
	relax() and decrease_slope() are adjusted for the special behaviour of this site. 
	"""

	def __init__(self, parent):
		Column.__init__(self, parent, parent.board_size-1)
		self.outflow_counter = 0


	def relax(self):
		"""
		Relaxes site in accordance with Oslo Model.
		"""
		self.parent.columns[self.column_num - 1].increase_slope()
		self.decrease_slope()
		self.change_threshold(self.parent.threshold_prob)
		self.parent.increment_outflow()
		self.parent.recurrence()

	def decrease_slope(self):
		"""
		Decreases the slop by 1. (special case)
		"""
		self.slope -= 1