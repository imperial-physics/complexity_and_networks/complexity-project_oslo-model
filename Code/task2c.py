import simulation
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy.optimize import fmin_tnc
from scipy.stats import linregress


board_sizes = [8,16,32]

def scaling_form(board_sizes, a_0, a_01, w_1):
	"""
	Returns scaling correction for a list of heights
	"""
	return(a_0 - a_01 * np.array(board_sizes)**w_1)

def power_law_det(board_sizes, a, b, c):
	return(a* np.array(board_sizes)**b + c)

def log_fit(board_sizes, grad, intercept):
	return(grad * np.array(board_sizes) + intercept)

def height_and_sigma(sim):

	print("Time average height of recurrent configurations: ", sim.average_h_power_k(), sep="")
	print("Standard deviation of height of recurrent configurations: ", sim.standard_deviation(), sep="")

def plot_height(ensemble):

	height, unscaled = plt.subplots()
	unscaled.set_xlabel("Size")
	unscaled.set_ylabel("Height")
	height.show()

	board_sizes = [sim.board.board_size for sim in ensemble]
	average_heights = [sim.average_h_power_k(k=1) for sim in ensemble]

	unscaled.plot(board_sizes,average_heights)
	height.show()

def plot_slope(ensemble):

	height, scaled = plt.subplots()
	scaled.set_xlabel("Size")
	scaled.set_ylabel("Height (scaled to system size)")

	board_sizes = [sim.board.board_size for sim in ensemble]
	average_slopes = [sim.average_h_power_k(k=1)/sim.board.board_size for sim in ensemble]

	scaled.plot(board_sizes,average_slopes)

	scalings = curve_fit(scaling_form, board_sizes, average_slopes, p0=(1.67,0.5,-0.5))[0]
	print(scalings)

	scaled.plot(board_sizes,scaling_form(board_sizes, scalings[0], scalings[1], scalings[2]))
	height.show()

def plot_height_sigma(ensemble):

	sigma, ax = plt.subplots()
	ax.set_xlabel(r"$L$")
	ax.set_ylabel(r"$\sigma_h$")
	ax.set_yscale('log')
	ax.set_xscale('log')

	board_sizes = [sim.board.board_size for sim in ensemble]
	sigmas = [sim.standard_deviation() for sim in ensemble]
	fit = curve_fit(power_law_det, board_sizes, sigmas, p0=(1, 0.2, -0.5))[0]
	logfit = curve_fit(log_fit, np.log(board_sizes), np.log(sigmas))[0]
	print(fit)
	print(logfit)
	ax.plot(board_sizes, sigmas, label="$Data$")
	#ax.plot(board_sizes, power_law_det(board_sizes,fit[0],fit[1], fit[2]))
	ax.plot(board_sizes, np.exp(logfit[1])*board_sizes**logfit[0], label=r"$\sigma_h = %.3g \times L^{%.3g} $" % (np.exp(logfit[1]),logfit[0]))
	#print(fit)
	ax.legend(loc='best').draggable()
	sigma.show()

def plot_height_sigma_cts(ensemble):

	sigma, ax = plt.subplots()
	ax.set_xlabel(r"$L$")
	ax.set_ylabel(r"$L^{-0.237}\sigma_h$")
	#ax.set_yscale('log')
	#ax.set_xscale('log')

	board_sizes = [sim.board.board_size for sim in ensemble]
	scaled_sigmas = [sim.board.board_size**-0.237 * sim.standard_deviation() for sim in ensemble]
	ax.plot(board_sizes, scaled_sigmas, label="$Data$")
	#ax.legend(loc='best').draggable()
	sigma.show()

def plot_slope_sigma(ensemble):

	sigma, ax = plt.subplots()
	ax.set_xlabel("Size")
	ax.set_ylabel("Slope standard deviation")
	board_sizes = [sim.board.board_size for sim in ensemble]
	sigmas = [sim.standard_deviation() for sim in ensemble]
	ax.plot(board_sizes, np.array(sigmas)/(np.array(board_sizes)))
	sigma.show()


def curve_fitting(ensemble):
	height, unscaled = plt.subplots()
	unscaled.set_xlabel(r"$\ln{\left(L\right)}$")
	unscaled.set_ylabel(r"$\ln{\left(a_0L - \langle h(t;L) \rangle_t\right)}$")

	a_0 = 1.75
	board_sizes = [sim.board.board_size for sim in ensemble]
	heights = [sim.average_h_power_k(k=1) for sim in ensemble]
	x = fmin_tnc(linregress_func, a_0, args = (heights, board_sizes), bounds=[(1.72, 1.75)], approx_grad=True)
	print("x=", x[0][0], "+-", x[1])
	print(x)

	m, c, r, _, m_err, = linregress(np.log(board_sizes), np.log(abs(x[0][0] * np.array(board_sizes) - np.array(heights))))
	print(1-m, "+-", m_err)

	unscaled.plot(np.log(board_sizes),np.log(abs(x[0][0] * np.array(board_sizes) - np.array(heights))))
	height.show()


def linregress_func(a_0, heights, board_sizes):
	print(a_0)
	x = np.log(board_sizes)
	y = np.log(abs(a_0 * np.array(board_sizes) - np.array(heights)))

	m, c, r, _, _, = linregress(x,y)

	return(-r*r)