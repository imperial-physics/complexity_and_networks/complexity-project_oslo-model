import simulation
import matplotlib.pyplot as plt
import numpy as np
import log_bin3

def plot_probability_single(sim):
	figure, ax = plt.subplots()
	ax.set_xlabel("Outflow size")
	ax.set_ylabel("Outflow probability")
	ax.set_yscale('log')
	ax.set_xscale('log')

	outflow_size = sim.grain_outflows()
	outflow_probabilities = sim.outflow_probabilities(outflows=outflow_size)

	##sorts and removes duplicates of list
	outflow_size_list = list(set(outflow_size))

	##Plotting calculated probabilities of outflow sizes, d=0 omitted
	ax.scatter(outflow_size_list, outflow_probabilities)

	##Binning the data using given code
	bin_centres, prob_density = log_bin3.log_bin(outflow_size, a=1.3, datatype="integer", drop_zeros=False)
	ax.plot(bin_centres, prob_density)

	figure.show()

def plot_probability_ensemble(ensemble):
	figure, ax = plt.subplots()
	ax.set_xlabel("Outflow size")
	ax.set_ylabel("Outflow probability")
	ax.set_yscale('log')
	ax.set_xscale('log')

	for sim in ensemble:

		outflow_size = sim.grain_outflows()
		outflow_probabilities = sim.outflow_probabilities(outflows=outflow_size)

		##Binning the data using given code
		bin_centres, prob_density = log_bin3.log_bin(outflow_size, a=1.35, datatype="integer", drop_zeros=False)
		ax.plot(np.array(bin_centres)/sim.board.board_size**1.25, prob_density*sim.board.board_size**1.3, label="L = %s" % sim.board.board_size)

	ax.legend(loc='best').draggable()
	figure.show()