import simulation
import matplotlib.pyplot as plt
import numpy as np
import log_bin3


def plot_probability(ensemble, N=10**6):
	figure, ax = plt.subplots()
	ax.set_xlabel("Avalanche size")
	ax.set_ylabel("Avalanche probability")
	ax.set_yscale('log')
	ax.set_xscale('log')

	for sim in ensemble:
		

		if N > sim.time - sim.safe_recurrence_time:
			print("Error: please use a smaller value of N")
			return(1)

		avalanche_size = sim.recurrent_avalanches()[-N:]
		avalanche_probabilities = sim.avalanche_probabilities(avalanches=avalanche_size)

		##Binning the data using given code
		bin_centres, prob_density = log_bin3.log_bin(avalanche_size, a=1.3, datatype="integer", drop_zeros=False)
		ax.plot(bin_centres, prob_density, label="L = %s" % sim.board.board_size)

	ax.legend(loc='best').draggable()
	figure.show()
