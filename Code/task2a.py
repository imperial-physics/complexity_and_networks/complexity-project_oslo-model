import simulation
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit



def linear_fit(lengths, grad, intercept):
	return(grad * np.array(lengths) + intercept)

def recurrent_height(ensemble):
	return([sim.average_h_power_k(k=1) for sim in ensemble])

def critical_times(ensemble):
	return([sim.recurrence_time for sim in ensemble])


def height_scaling(ensemble):

	lengths = np.log([sim.board.board_size for sim in ensemble])
	heights = np.log(recurrent_height(ensemble))

	fit = curve_fit(linear_fit, lengths, heights)
	print(fit[0][0])
	print(fit[1][0,0]**0.5)

	height, ax = plt.subplots()
	ax.set_xlabel(r"$L$")
	ax.set_ylabel(r"$ \langle h(t;L) \rangle_t$")
	#ax.scale("log")

	ax.loglog(lengths, heights,label="Data")
	ax.loglog(lengths, linear_fit(lengths, fit[0][0], fit[0][1]),label="Fit")


	ax.legend(loc='best').draggable()
	height.show()

def time_scaling(ensemble):

	lengths = np.log([sim.board.board_size for sim in ensemble])

	times = np.log(critical_times(ensemble))


	fit = curve_fit(linear_fit, lengths, times)
	print(fit[0][0])
	print(fit[1][0,0]**0.5)

	height, ax = plt.subplots()
	ax.set_xlabel(r"L")
	ax.set_ylabel(r"$t_c$")
	#ax.scale("log")

	ax.loglog(lengths, times,label="Data")
	ax.loglog(lengths, linear_fit(lengths, fit[0][0], fit[0][1]),label="Fit")


	ax.legend(loc='best').draggable()
	height.show()

def time_scaling_effects(ensemble):

	lengths = [sim.board.board_size for sim in ensemble]

	times = critical_times(ensemble)

	height, ax = plt.subplots()
	ax.set_xlabel("Time")
	ax.set_ylabel("Height")
	#ax.scale("log")

	ax.plot(lengths, np.array(times)/(np.array(lengths)**2),label="Data")

	ax.legend(loc='best').draggable()
	height.show()



def plot_heights(ensemble):

	height, ax = plt.subplots()
	loglog, ax2 = plt.subplots()
	
	ax.set_xlabel("Time")
	ax.set_ylabel("Height")
	ax2.set_xlabel("Time")
	ax2.set_ylabel("Height")

	for sim in ensemble:
		ax.plot(range(sim.time), sim.find_pile_height(), label="L = %s" % sim.board.board_size)
		ax2.loglog(range(sim.time), sim.find_pile_height(), label="L = %s" % sim.board.board_size)
		ax.axvline(x=sim.recurrence_time, linewidth=0.5)

	ax.legend(loc='best').draggable()
	ax2.legend(loc='best').draggable()
	height.show()
	loglog.show()

