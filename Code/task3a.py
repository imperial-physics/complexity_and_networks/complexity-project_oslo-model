import simulation
import matplotlib.pyplot as plt
import numpy as np
import log_bin3


def plot_probability(sim, N=10**5, a=1.3):
	figure, ax = plt.subplots()
	ax.set_xlabel(r"Avalanche size, $s$")
	ax.set_ylabel(r"Avalanche probability")
	ax.set_yscale('log')
	ax.set_xscale('log')

	if N > sim.time - sim.safe_recurrence_time:
		print("Error: please use a smaller value of N")
		return(1)

	avalanche_size = sim.recurrent_avalanches()[-N:]
	avalanche_probabilities = sim.avalanche_probabilities(avalanches=avalanche_size)

	##sorts and removes duplicates of list
	avalanche_size_list = list(set(avalanche_size))

	##Plotting calculated probabilities of avalanche sizes, s=0 omitted
	ax.scatter(avalanche_size_list[1:], avalanche_probabilities[1:], label=r"Probability distribution, $P_N(s;L)$")

	##Binning the data using given code
	bin_centres, prob_density = log_bin3.log_bin(avalanche_size, a=a, datatype="integer", drop_zeros=False)
	ax.plot(bin_centres, prob_density, label=r"Binned Probability distribution, $\tilde{P}_N(s;L)$")

	ax.legend(loc='best').draggable()
	figure.show()