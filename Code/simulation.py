import oslo
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt


class Simulation():
	"""
	Class contraining a single instance of the Oslo Model.
	Acts to extract data from board state of contained Board
	and perform common manipulations on the data.

	Arguments:
	board_size(int): Number of columns in the Simulation's Board
	threshold_prob(float): Threshold probability dictating the critical thresholds of the Board
	"""

	def __init__(self, board_size, threshold_prob = 0.5):
		self.board = oslo.Board(board_size, threshold_prob)
		self.time = 0
		self.pile_height = []
		self.avalanche_sizes = []
		self.grain_outflows_list = []
		self.recurrence_time, self.safe_recurrence_time = 0, 0

	####################################################################################
	## Oslo Model simulation and data collection
	####################################################################################

	def simulate(self, steps=1, until_recurrence=False):
		"""
		Simulates time passing
		Automatically checks for any unstable sites and relaxs them
		If board is stable then it is driven.
		"""

		if until_recurrence == True:
			print("Iterating until recurrence")
			while (self.time < self.safe_recurrence_time or self.board.recurrence_flag == False):
				self.iterate()
			print("Board is now in a safely recurrent state after adding", self.time, "grains of rice")
		else:
			print("Iterating for", steps, "grains")
			i = 0
			while (i < steps):
				self.iterate()
				print("%0.2f" % (i/steps*100), "\b% Complete", end="\r")
				i += 1

			print("\n", end="\r")

	def iterate(self):
		#Drives board then relaxes to stability
		self.board.drive()
		avalanche_size = 0
		self.board.grain_num = 0
		while (self.board.relax_board()):
			avalanche_size += 1

		#Reads board state and extracts data
		self.take_data()
		self.avalanche_sizes.append(avalanche_size)
		
		#advances time			
		self.time += 1

	def take_data(self):
		"""
		Reads board state and extracts data
		Reads:
		Pile height
		Time when system entered recurrent state
		Time when system safely in recurrent region
		Number of grains outflowing from system
		"""

		## Pile height
		self.pile_height.append(self.board.pile_height)

		## Recurrence time
		if self.board.recurrence_flag == True and self.recurrence_time == 0 :
			self.recurrence_time = self.time
			self.safe_recurrence_time = int( 1.1 * self.recurrence_time)

		## Grain outflow
		if self.board.read_outflow() != 0:
			self.grain_outflows_list.append(self.board.read_outflow())
		self.board.reset_outflow()

	####################################################################################
	## Data manipulation
	####################################################################################

	## Height metrics

	def find_pile_height(self):
		return(self.pile_height)

	def time_average(self, pile_height, period=50):
		"""
		Performs a moving average on given height list with the period datapoints
		before and after the one being averaged.
		"""
		averaged_height = []
		for i in range(self.time - 2 * period):
			averaged_height.append(float(sum(pile_height[i:i + 2*period])/(2*period+1)))
		return(averaged_height)

	## "Data Collapse" metrics

	def scaled_time(self, time=None, period=0):
		"""
		Scales time to be in terms of the critial time
		by dividing by the length^2 of the Board
		"""
		if time == None:
			time = self.time
		if period == 0:
			return(np.arange(time)/self.board.board_size**2)
		else:
			return(np.arange(period, time - period)/self.board.board_size**2)


	def scaled_height(self, heights=None):
		"""
		Scales height to be in terms of the final reccurent height
		by dividing by the length of the system
		"""
		if heights == None:
			heights=self.pile_height
		return(np.array(heights)/self.board.board_size)

	## Recurrence height metrics

	def get_recurrence_heights(self, heights = None):
		"""
		Returns a list of heights corresponding to the safely reccurent configurations of the system
		"""
		if heights == None:
			heights = self.find_pile_height()
		recurrence_heights = heights[self.safe_recurrence_time:]
		return(recurrence_heights)

	def average_h_power_k(self, heights = None, k=1):
		"""
		Returns a sum of the kth powers of  a list of the system heights.
		By default, uses the heights of the system once it is in the safely recurrent state.
		"""
		if heights == None:
			heights = self.get_recurrence_heights()
		heights_power_k = [h**k for h in heights]
		sum_heights = float(sum(heights_power_k))/len(heights)
		return(sum_heights)

	def standard_deviation(self, heights=None):
		"""
		Helper function to calculate the standard deviation of a list of heights
		Equivalent to sqrt(average_h_power_k(k=2) - (average_h_power_k(k=1))**2)
		"""
		if heights == None:
			heights = self.get_recurrence_heights()
		sigma = sqrt(self.average_h_power_k(heights,2) - self.average_h_power_k(heights,1)**2)
		return(sigma)


	## Recurrence height probability distribution calculations

	def height_prob(self, height, heights=None):
		"""
		Calculates the probability of a given height occurring using a given list of heights
		"""
		if heights == None:
			heights = self.get_recurrence_heights()

		probability = float(heights.count(height))/len(heights)
		return(probability)

	def height_occurrences(self, heights=None):
		"""
		Returns a list of tuples containing the Board height and the number of times that height has occurred
		Returns values for all heights between the maximum and minimum observed heights.
		"""
		if heights == None:
			heights = self.get_recurrence_heights()

		height_counts=[]
		for height in range(min(heights), max(heights)+1):
			height_counts.append((height,heights.count(height)))
		return(height_counts)

	def height_probabilities(self, heights=None):
		"""
		Returns a list of the probabilities of observing a configuration of a given height, giving a probability distribution.
		Returns values for all heights between the maximum and minimum observed heights.
		"""
		if heights == None:
			heights = self.get_recurrence_heights()

		height_probabilities=[]
		for height in range(min(heights), max(heights)+1):
			height_probabilities.append(self.height_prob(height,heights))
		return(height_probabilities)

	## Avalanche Metrics

	def avalanches(self):
		return(self.avalanche_sizes)

	def recurrent_avalanches(self):
		recurrent_avalanches = self.avalanches()[self.safe_recurrence_time:]
		return(recurrent_avalanches)

	def avalanche_prob(self, size, avalanches, N):
		"""
		Calculates the probability of a given size avalanche occurring using a given list of avalanche sizes
		"""
		if avalanches == None:
			avalanches = self.recurrent_avalanches()

		probability = float(avalanches.count(size))/len(avalanches)
		return(probability)

	def avalanche_probabilities(self, avalanches=None, N=None):
		"""
		Returns a list of the probabilities of observing a configuration of a given height, giving a probability distribution.
		Returns values for all sizes between the maximum and minimum observed sizes.
		"""
		if avalanches == None:
			avalanches = self.recurrent_avalanches()
			N = len(avalanches)

		avalanche_sizes = list(set(avalanches))
		avalanche_probabilities=[]
		for size in avalanche_sizes:
			avalanche_probabilities.append(self.avalanche_prob(size, avalanches, N))
		return(avalanche_probabilities)

	def avalanche_k_moment(self, avalanches=None, k=1):
		"""
		Returns a sum of the kth powers of a list of the avalanche sizes.
		By default, uses the avalanches of the system once it is in the safely recurrent state.
		"""
		if avalanches == None:
			avalanches = self.recurrent_avalanches()
		return(sum([int(i)**k for i in avalanches])/len(avalanches))

	## Outflow Metrics

	def grain_outflows(self):
		"""
		Returns a list of the numbers of grains leaving the system in a single avalanche
		"""
		return(self.grain_outflows_list)

	def outflow_prob(self, size, outflows, N):
		"""
		Calculates the probability of a given outflow occurring using a given list of outflows
		"""
		if outflows == None:
			outflows = self.grain_outflow()

		probability = float(outflows.count(size))/len(outflows)
		return(probability)

	def outflow_probabilities(self, outflows=None, N=None):
		"""
		Returns a list of the probabilities of observing a given outflow, giving a probability distribution of the sizes of outflows.
		Returns values for all sizes between the maximum and minimum observed outflow.
		"""
		if outflows == None:
			outflows = self.grain_outflows()
			N = len(outflows)

		outflow_sizes = list(set(outflows))
		outflow_probabilities=[]
		for size in outflow_sizes:
			outflow_probabilities.append(self.outflow_prob(size, outflows, N))
		return(outflow_probabilities)